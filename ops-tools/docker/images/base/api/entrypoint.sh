#!/bin/bash

echo ***** Start app  *****

if [ $ENV == "local" ]
then
  echo ***** Install dependencies  *****
  npm i

  echo ***** In dev mod  *****
  npm run dev
else
  echo ***** In prod mod  *****
  npm run start
fi
